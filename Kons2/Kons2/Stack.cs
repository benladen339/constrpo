﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kons2
{
    public class Stack:IStack
    {
        List2 top ; 
        int count;
        public Stack()
        {
            top = new List2();
            top.x = 0;
            count=0;
        }
        public Stack(int x)
        {
            top = new List2();
            top.x = x;
            count++;
        }
        public Stack(int[] x)
        {
            top = new List2();
            top.x = x[0];
            for (int i = 1; i < x.Length; i++)
            {
                List2 value = new List2();
                value.x = x[i];
                value.prev = null;
                value.next = top;
                top.prev = value;
                top = value;
            }
            count = x.Length;

        }

        public void push(int x)
        {
            if (count == 0)
            {
                top.x = x;
                top.prev = null;
                top.next = null;
                count++;
            }
            else
            {
                List2 value = new List2();
                value.x = x;
                value.next = top;
                value.prev = null;
                top.prev = value;
                top = value;
                count++;
            }
        }


        public int pop()
        {
            if (count == 0)
            {
                throw new ArgumentException("Стек пуст");
            }
            else
            {
                int x = top.x;
                top = top.next;
                top.prev = null;
                count--;
                return x;
            }
        }

        public int Count
        {
            get
            {
                return count;
            }
        }
    }



    class List2
    {
        public int x;
        public List2 next;
        public List2 prev;

    }
    }
