﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kons2
{
    interface IQueue
    {
        int dequeue();
        void enqueue(int a);
    }
    interface IStack
    {
        int pop();
        void push(int a);
    }
}
