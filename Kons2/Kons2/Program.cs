﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kons2
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack s = new Stack();
            IStack stackNumber = s as IStack;
            if (stackNumber != null)
            {
                stackNumber.push(1);
                stackNumber.push(2);
                stackNumber.push(3);
                stackNumber.push(4);
                stackNumber.push(5);
                Console.WriteLine(stackNumber.pop());
                Console.WriteLine(stackNumber.pop());
                Console.WriteLine(stackNumber.pop());
                Console.WriteLine(stackNumber.pop());
            }

            Queue que = new Queue();
            IQueue QueueNumber = que as IQueue;
            if (QueueNumber != null)
            {
                QueueNumber.enqueue(1);
                QueueNumber.enqueue(2);
                QueueNumber.enqueue(3);
                QueueNumber.enqueue(4);
                QueueNumber.enqueue(5);
                Console.WriteLine(QueueNumber.dequeue());
                Console.WriteLine(QueueNumber.dequeue());
                Console.WriteLine(QueueNumber.dequeue());
                Console.WriteLine(QueueNumber.dequeue());
            }
            Console.ReadKey();
        }
    }
}
