﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kons2
{
    public class Queue:IQueue
    {
        List1 top; 
        int count;
        public Queue()
        {
            top = new List1();
            top.x = 0;
            count=0;
        }
        public Queue(int x)
        {
            top = new List1();
            top.x = x;
            count++;
        }
        public Queue(int[] x)
        {
            top = new List1();
            top.x = x[0];
            for (int i = 1; i < x.Length; i++)
            {
                List1 value = new List1();
                value.x = x[i];
                value.prev = top;
                value.next = null;
                top.next = value;
                top = value;
            }
            count = x.Length;

        }

        public void enqueue(int x)
        {
            if (count == 0)
            {
                top.x = x;
                top.prev = null;
                top.next = null;
                count++;
            }
            else
            {
                List1 value = new List1();
                List1 now = top;

                while(now.prev != null)
                {
                    now = now.prev;
                }
                value.x = x;
                value.next = now;
                value.prev = null;
                now.prev = value;
                count++;
            }
        }


        public int dequeue()
        {
            if (count == 0)
            {
                throw new ArgumentException("Стек пуст");
            }
            else
            {
                int x = top.x;
                top = top.prev;
                top.next = null;
                count--;
                return x;
            }
        }
        public int Count
        {
            get
            {
                return count;
            }
        }
    }

    class List1
    {
        public int x;
        public List1 next;
        public List1 prev;
    }
    
}
